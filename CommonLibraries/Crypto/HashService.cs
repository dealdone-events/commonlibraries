using System.Security.Cryptography;
using System.Text;

namespace CommonLibraries.Crypto
{
    public class HashService
    {
        public string HashWithSha256(string value, string secret)
        {
            var encoder = new SHA256Managed();
            var hash = new StringBuilder();
            byte[] hashBytes = encoder.ComputeHash(Encoding.UTF8.GetBytes(value + secret));
            foreach (byte theByte in hashBytes)
            {
                hash.Append(theByte.ToString("x2"));
            }

            return hash.ToString();
        }
    }
}