﻿using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using CommonLibraries.Guards;
using CommonLibraries.Jwt;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CommonLibraries.Authentication
{
    public class TrustedJwtAuthenticationHandler : AuthenticationHandler<TrustedJwtAuthenticationOptions>
    {
        public TrustedJwtAuthenticationHandler(IOptionsMonitor<TrustedJwtAuthenticationOptions> options, 
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            BaseJwtService jwtService) : base(options, logger, encoder, clock)
        {
            _jwtService = Check.NotNull(jwtService, nameof(jwtService));
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey(TrustedJwtAuthenticationSettings.TokenHeaderName))
            {
                return AuthenticateResult.Fail("Header not found");
            }
            
            string token = Request.Headers[TrustedJwtAuthenticationSettings.TokenHeaderName];
            var user = _jwtService.ParseUser(token);
            
            var claimsIdentity = new ClaimsIdentity(nameof(TrustedJwtAuthenticationHandler));
            claimsIdentity.AddClaim(new Claim("Id", user.Id.ToString()));
            claimsIdentity.AddClaim(new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role.ToString()));
            return AuthenticateResult.Success(new AuthenticationTicket(new ClaimsPrincipal(claimsIdentity), Scheme.Name));
        }

        private readonly BaseJwtService _jwtService;
    }
}