﻿using Microsoft.Extensions.DependencyInjection;

namespace CommonLibraries.Authentication
{
    public static class TrustedJwtAuthenticationExtension
    {
        public static IServiceCollection AddTrustedJwtAuthentication(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddAuthentication(TrustedJwtAuthenticationSettings.SchemeName)
                .AddScheme<TrustedJwtAuthenticationOptions, TrustedJwtAuthenticationHandler>(TrustedJwtAuthenticationSettings.SchemeName, _ => { });
            return serviceCollection;
        }
    }
}