﻿namespace CommonLibraries.Authentication
{
    public static class TrustedJwtAuthenticationSettings
    {
        public static string SchemeName => "TrustedJwtScheme";
        
        internal static string TokenHeaderName => "token";
    }
}