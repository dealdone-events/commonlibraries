using System;
using CommonLibraries.Guards;
using CommonLibraries.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CommonLibraries.Filters
{
    public class GlobalExceptionFilter : IExceptionFilter
    {
        public GlobalExceptionFilter(IHostEnvironment hostEnvironment, ILogger<GlobalExceptionFilter> logger)
        {
            _hostEnvironment = Check.NotNull(hostEnvironment, nameof(hostEnvironment));
            _logger = Check.NotNull(logger, nameof(logger));
        }

        public void OnException(ExceptionContext context)
        {
            context.Result = ((dynamic)this).ConvertException(context.HttpContext.Request, (dynamic)ExtractException(context.Exception));
        }

        private ContentResult ConvertException(HttpRequest request, ArgumentException ex) => CreateErrorResponse(request, ex, ex.Message, 400);

        private ContentResult ConvertException(HttpRequest request, RepositoryException ex) => CreateErrorResponse(request, ex, ex.Message, 404);

        private ContentResult ConvertException(HttpRequest request, Exception ex) => CreateErrorResponse(request, ex, ex.Message, 500);

        protected ContentResult CreateErrorResponse(HttpRequest request,
            Exception exception,
            string message,
            int statusCode)
        {
            _logger.LogError(exception,"Request converted to the result with status code: {StatusCode} and message: {Message}", statusCode, message);

            return new ContentResult
            {
                Content = message,
                StatusCode = statusCode
            };
        }

        private Exception ExtractException(Exception ex)
        {
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
            }

            return ex;
        }

        private readonly IHostEnvironment _hostEnvironment;
        private readonly ILogger _logger;
    }
}