using System;
using CommonLibraries.Identities;

namespace CommonLibraries.Jwt
{
    public class UserJwtModel
    {
        public Guid? Id { get; set; }
        
        public UserRole Role { get; set; }
    }
}