using JWT;
using JWT.Serializers;
using Newtonsoft.Json;

namespace CommonLibraries.Jwt
{
    public class BaseJwtService
    {
        public UserJwtModel ParseUser(string token)
        {
            IJwtDecoder decoder = new JwtDecoder(new JsonNetSerializer(), new JwtBase64UrlEncoder());
            var decodedClaims = decoder.DecodeToObject(token);
            return JsonConvert.DeserializeObject<UserJwtModel>(decodedClaims["User"].ToString(), new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }
    }
}