namespace CommonLibraries.Identities
{
    public enum UserRole
    {
        Authorized = 0,
        Pro = 1,
        Unauthorized = 2
    }
}