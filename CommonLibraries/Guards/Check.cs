﻿using System;

namespace CommonLibraries.Guards
{
    public static class Check
    {
        public static T NotNull<T>(T value, string name)
            where T : class
        {
            if (value == null)
            {
                throw new ArgumentException($"{name} can not be null");
            }

            return value;
        }
        
        public static string NotNullOrEmpty(string value, string name)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentException($"{name} can not be null");
            }

            return value;
        }
    }
}